ARG ASTRO_RUNTIME_VERSION=7.2.0
FROM quay.io/astronomer/astro-runtime:${ASTRO_RUNTIME_VERSION}-base

LABEL maintainer="Olivier Daneau <olivier.daneau@astronomer.io>"
# Add Private Pypi Registry
RUN pip config set global.trusted-host "pip.astronomer.io pypi.python.org pypi.org files.pythonhosted.org "
    pip config set global.timeout 20 && \
    pip install --upgrade setuptools

USER root
# Add Private SSL Certificates
COPY include/all-certs.crt /usr/local/share/ca-certificates
RUN chmod 644 /usr/local/share/ca-certificates/all-certs.crt && \
    update-ca-certificates

RUN apt-install-and-clean build-essential git gnupg jq python3-dev python-dev
# Add MSSQL Apt repository
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/debian/11/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN ACCEPT_EULA=Y apt-install-and-clean msodbcsql18 mssql-tools18 unixodbc unixodbc-dev kstart krb5-user krb5-config libpam-krb5

COPY include/krb5.conf /etc/krb5.conf
COPY include/odbc.ini /etc/odbc.ini

USER astro
# Disable SSL Verification
RUN echo --insecure > ~/.curlrc && \
    git config --global http.sslverify false

# Prebuild Venvs for ExternalPythonOperator from virtualenvs.json config
COPY venv.sh /home/astro/venv.sh
ONBUILD COPY virtualenvs.json /home/astro/virtualenvs.json
ONBUILD ENV PYENV_ROOT="/home/astro/.pyenv"
ONBUILD ENV PATH=${PYENV_ROOT}/bin:${PATH}
ONBUILD RUN /home/astro/venv.sh