# Astro Custom Runtime Docker Image

This project showcases how to build a custom Astro Runtime image to add private Pypi Repository, private SSL
certificates, ODBC drivers and more.
The Dockerfile includes a script (`venv.sh`) to dynamically create python virtual environments from
configs (`virtualenvs.json`) to use with PythonExternalPythonOperator.

Astro Custom Runtime Images come in two variants:

1. astro-custom-runtime:x.x.x-base
2. astro-custom-runtime:x.x.x

The base image is meant as a building block for the second image.

## Build Custom Runtime Image Base

Run this command to build the Custom Runtime Base Image based on Astro Runtime Base

```shell
docker build --pull -t astro-custom-runtime:7.3.0-base --build-arg ASTRO_RUNTIME_VERSION=7.3.0 .
```

To test the image, launch a container with this command

```shell
docker run --rm -it --entrypoint bash astro-custom-runtime:7.3.0-base
```

## Build Custom Runtime Image

The astro-custom-runtime image incorporate Docker ONBUILD commands to copy your project directory
(i.e. your packages.txt, requirements.txt, dags, etc.) and scaffold a Dockerfile
so you can more easily pass those files to the containers running each core Airflow component
(Scheduler, Webserver, Airflow Database) to your local machine or to a Deployment on Astronomer.

The Dockerfile.onbuild will take care of:

- Installing Linux packages listed in packages.txt
- Installing Python dependencies listed in requirements.txt
- Creating Python Virtualenvs listed in virtualenvs.json

Run this command to build the Custom Runtime Base Image based on Astro Runtime Base

```shell
docker build -t astro-custom-runtime:7.3.0 --file ./Dockerfile.onbuild --build-arg CUSTOM_RUNTIME_VERSION=7.3.0-base .
```

To test the image, launch a container with this command

```shell
docker run --rm -it --entrypoint bash astro-custom-runtime:7.3.0
```

## Adding Python Virtual environments

Add new entries to `virtualenvs.json` like this:

```json
[
  {
    "venv_id": "my_env",
    "python_version": "3.9.10",
    "requirements": [
      "cython==0.29.32",
      "oauthlib==3.2.1",
      "requests-oauthlib==1.3.1"
    ]
  },
  {
    "venv_id": "datascience",
    "python_version": "3.9.10",
    "requirements": [
      "scikit-learn",
      "pandas"
    ]
  }
]
```

## Using the Astro Custom Runtime Image in your project

First create your Astro project if not already done:

```shell
astro dev init
```

Then change the `FROM` clause in your Dockerfile to this:

```Dockerfile
FROM astro-custom-runtime:7.3.0
```

### Using the Virtual environments in your DAG

You can reference the virtual environments defined in `virtualenvs.json` in your DAG as follows:

````python
from airflow import DAG
from airflow.decorators import task
import pendulum

with DAG(
        dag_id="py_virtual_env",
        schedule=None,
        start_date=pendulum.datetime(2023, 1, 1, tz="UTC"),
        catchup=False,
        tags=["pythonvirtualenv"],
):


    @task.external_python(
        task_id="external_python",
        python="/home/astro/.pyenv/versions/my_env/bin/python",  # my_env must match venv_id in virtualenvs.json
    )
    def external_python():
        import pkg_resources
    
        ## Checking for the correct venv packages
        installed_packages = pkg_resources.working_set
        installed_packages_list = sorted(
            ["%s==%s" % (i.key, i.version) for i in installed_packages]
        )
        print(installed_packages_list)

    task_external_python = external_python()
````