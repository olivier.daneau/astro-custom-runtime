#!/usr/bin/env bash
curl https://pyenv.run | bash && eval "$(pyenv init -)"
readarray -t venvs < <(jq -c '.[]' /home/astro/virtualenvs.json) &&
  for venv in "${venvs[@]}"; do
    venv_id=$(jq -r '.venv_id' <<<"$venv")
    python_version=$(jq -r '.python_version' <<<"$venv")
    reqs_array=$(jq -r '.requirements' <<<"$venv")
    readarray -t requirements < <(jq -c -r '.[]' <<<"$reqs_array")
    pyenv install -s "$python_version" &&
      pyenv virtualenv "$python_version" "$venv_id" &&
      pyenv activate "$venv_id" &&
      pip install --no-cache-dir --upgrade "${requirements[@]}"
  done